<?php
require_once('Animal.php');
require_once('Frog.php');


$sheep = new Animal("shaun");

echo "Name  :" . $sheep->name . "<br>"; // "shaun"
echo "Legs  :" . $sheep->legs . "<br>"; // 4
echo "Cold blooded  :" . $sheep->cold_blooded . "<br><br>"; // "no"


$kodok = new Frog("buduk");
echo "Name  :" . $kodok->name . "<br>"; 
echo "Legs  :" . $kodok->legs . "<br>"; 
echo "Cold blooded  :" . $kodok->cold_blooded . "<br>"; 
echo "Jump  :" . $kodok->jump() . "<br><br>"; // "hop hop"

?>