<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
   public function register()
   {
    return view('page.register');
   }

   public function welcome(Request $request)
   {
    // dd($request);
    //fname diambil dari name di view
    $namadepan      = $request->input('fname');
    $namabelakang   = $request->input('lname');
    $jnskelamin     = $request->input('gender');
    $national       = $request->input('nasional');
    $bahasa         = $request->input('bhs');
    $keterangan     = $request->input('box');

    $data = [
        'namadepan'      => $namadepan,
        'namabelakang'   => $namabelakang,
        'jnskelamin'     => $jnskelamin,
        'national'       => $national,
        'bahasa'         => $bahasa,
        'keterangan'     => $keterangan,
    ];

    return view('page.welcome',$data);
   }
}
