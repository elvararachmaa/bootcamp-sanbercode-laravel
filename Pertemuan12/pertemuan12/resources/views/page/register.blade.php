@extends('layouts.master')

@section('title')
    Buat Account Baru
@endsection

@section('content')
    <h4>Sign Up Form</h4>
    <form action="/welcome" method="POST">
        {{-- csrf itu bawaan dari laravel yg tujuannya agar terhindar dari serangan csrf attack yg mana
            data kita akan diambil berdasarkan url yang kita masukan kedalam inputannya  --}}
        @csrf
        <label>First Name :</label><br>
        <input type="text" name="fname"><br><br>

        <label>Last Name :</label><br>
        <input type="text" name="lname"><br><br>

        <label>Gender :</label><br>
        <input type="radio" name="gender" value="Man">Man<br>
        <input type="radio" name="gender" value="Woman">Woman<br>
        <input type="radio" name="gender" value="Other">Other<br><br>

        <label>Nationality :</label>
        <select name="nasional" id="nasional">
            <option value="indo">Bahasa Indonesia</option>
            <option value="singapura">Singapura</option>
            <option value="malay">Malaysia</option>
            <option value="korea">Korea</option>
        </select><br><br>

        <label>Language Spoken :</label><br>
        <input type="checkbox" name="bhs" value="Bahasa Indonesia">Bahasa Indonesia<br>
        <input type="checkbox" name="bhs" value="English">English<br>
        <input type="checkbox" name="bhs" value="Arabic">Arabic<br>
        <input type="checkbox" name="bhs" value="Japanese">Japanese<br><br>

        <label>Box</label><br>
        <textarea id="box" name="box" rows="7" cols="35"></textarea><br><br>

        <input type="submit" value="Sign Up">
    </form>
@endsection
