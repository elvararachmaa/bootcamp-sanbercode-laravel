@extends('layouts.master')

@section('title')
    Detail Cast
@endsection

@section('content')
    <div class="row">
        <div class="col-12">
            <a href="/cast" type="button" class="btn btn-danger float-right">Kembali</a>
        </div>
    </div>

    <div class="card card-warning">
        <div class="card-header">
            <h3 class="card-title">Detail Cast</h3>
        </div>

        <div class="card-body">
            @csrf
            <div class="row">
                <div class="col-sm-12">
                    <div class="form-group">
                        <label>Nama</label>
                        <input type="text" class="form-control" id="nama" name="nama"
                            value="{{ $cast->nama }}">
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-12">
                    <div class="form-group">
                        <label>Umur</label>
                        <input type="text" class="form-control " id="umur" name="umur"
                            value="{{ $cast->umur }}">
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-12">
                    <div class="form-group">
                        <label>Biodata</label>
                        <textarea class="form-control" rows="3" id="bio" name="bio">{{ $cast->bio }}</textarea>
                    </div>
                </div>
            </div>
        </div>

    </div>
@endsection
