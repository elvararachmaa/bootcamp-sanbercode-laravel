@extends('layouts.master')

@section('title')
    Edit Cast
@endsection

@section('content')
    <div class="row">
        <div class="col-12">
            <a href="/cast" type="button" class="btn btn-danger float-right">Kembali</a>
        </div>
    </div>

    <div class="card card-warning">
        <div class="card-header">
            <h3 class="card-title">Edit Cast</h3>
        </div>

        <div class="card-body">
            <form action="/cast/{{ $cast->id }}" method="POST">
                @csrf
                @method('put')
                <div class="row">
                    <div class="col-sm-12">
                        <div class="form-group">
                            <label>Nama</label>
                            <input type="text" class="form-control @error('nama') is-invalid @enderror" id="nama"
                                name="nama" value="{{ $cast->nama }}">
                        </div>
                        @error('nama')
                            <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-12">
                        <div class="form-group">
                            <label>Umur</label>
                            <input type="text" class="form-control @error('umur') is-invalid @enderror" id="umur"
                                name="umur" value="{{ $cast->umur }}">
                        </div>
                        @error('umur')
                            <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-12">
                        <div class="form-group">
                            <label>Biodata</label>
                            <textarea class="form-control @error('bio') is-invalid @enderror" rows="3" id="bio" name="bio"
                                value="{{ $cast->bio }}">{{ $cast->bio }}</textarea>
                        </div>
                        @error('bio')
                            <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>
                </div>
                <button type="submit" class="btn btn-primary">Submit</button>
            </form>
        </div>

    </div>
@endsection
