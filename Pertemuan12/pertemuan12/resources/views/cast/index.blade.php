@extends('layouts.master')

@section('title')
    List Cast
@endsection

@push('js_listcast')
    <script src="{{ asset('/template/plugins/datatables/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('/template/plugins/datatables-bs4/js/dataTables.bootstrap4.js') }}"></script>
    <script>
        $(function() {
            $("#example1").DataTable({
                "responsive": true,
                "lengthChange": false,
                "autoWidth": false,
                "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
            }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
            $('#example2').DataTable({
                "paging": true,
                "lengthChange": false,
                "searching": false,
                "ordering": true,
                "info": true,
                "autoWidth": false,
                "responsive": true,
            });
        });
    </script>
@endpush

@section('content')
    <div class="row">
        <div class="col-12">
            <a href="/cast/create" type="button" class="btn btn-success float-right">Tambah Cast</a>
        </div>
    </div>

    <div class="card">
        <div class="card-header">
            <h3 class="card-title">Daftar Cast</h3>
        </div>

        <div class="card-body">
            <table id="example1" class="table table-bordered table-striped">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Nama</th>
                        <th>Umur</th>
                        <th>Biodata</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @forelse ($cast as $key => $item)
                        <tr>
                            <th scope="row">{{ $key + 1 }}</th>
                            <td>{{ $item->nama }}</td>
                            <td>{{ $item->umur }}</td>
                            <td>{{ $item->bio }}</td>
                            <td>
                                <form action="/cast/{{ $item->id }}" method="post">
                                    @method('delete')
                                    @csrf
                                    <a href="/cast/{{ $item->id }}" class="btn btn-info btm-sm">Detail</a>
                                    <a href="/cast/{{ $item->id }}/edit" class="btn btn-warning btm-sm">Edit</a>
                                    <input type="submit" class="btn btn-danger btm-sm" value="Delete">
                                </form>
                            </td>
                        </tr>
                    @empty
                        <h1>Data Cast kosong</h1>
                    @endforelse
                </tbody>
            </table>
        </div>

    </div>
@endsection
