<?php

use Illuminate\Support\Facades\Route;
use App\Http\controllers\HomeController;
use App\Http\controllers\AuthController;
use App\Http\controllers\TablesController;
use App\Http\controllers\CastController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

//entpoint/route
Route::get('/',[HomeController::class,'home']);
Route::get('/register', [AuthController::class, 'register']);
Route::post('/welcome', [AuthController::class, 'welcome']);

//pertemuan13
Route::get('/table',[TablesController::class,'tables']);
Route::get('/data-tables',[TablesController::class,'datatables']);

//pertemuan15
Route::get('/cast',[CastController::class,'index']); //menampilkan list data para pemain film (boleh menggunakan table html atau bootstrap card)
Route::get('/cast/create',[CastController::class,'create']); //menampilkan form untuk membuat data pemain film baru
Route::post('/cast',[CastController::class,'store']); //menyimpan data baru ke tabel Cast
Route::get('/cast/{id}',[CastController::class,'show']); //menampilkan detail data pemain film dengan id tertentu
Route::get('/cast/{id}/edit',[CastController::class,'edit']); //menampilkan form untuk edit pemain film dengan id tertentu
Route::put('/cast/{id}	',[CastController::class,'update']); //menyimpan perubahan data pemain film (update) untuk id tertentu
Route::delete('/cast/{id}',[CastController::class,'destroy']);	//menghapus data pemain film dengan id tertentu


// Route::get('/master', function(){
//     return view('layouts.master');
// });
